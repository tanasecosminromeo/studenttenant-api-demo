<?php
	set_time_limit(0);
	error_reporting(E_ALL);
	ini_set('display_errors', '1');

	if (isset($_GET['plain'])){ header("Content-type: text/plain"); }
	include "system/config.php";
	include "system/studenttenant.sdk.php";
	
	$st = new StudentTenant($_STConfig);
	
	echo "=========================== VERSION\n"; //No arguments are required
	print_r($st->call("version"));
	echo "\n===========================\n\n";
	
	/*echo "=========================== PROPERTY SEARCH\n"; //No arguments are required
	$params = array(
			//"bedrooms" => 4, #Default all number of bedrooms
			"location" => "", #Default is null. Can be a street name or a postcode. Not case sensitive
			"town"     => "London", #Default is null. Not case sensitive
			//"maxRent"  => 390, #Rent per person per month. Default is 0	
			//"minRent"  => 300, #Rent per person per month. Default is 99999
			//"images"   => true, #Show only properties with images. Default false
			//"virtualtour" => true, #Show only properties with virtual tours. Default is false
			//"accredited"  => true, #Show only accredited properties. Default is false
			//"lodgings"    => true, #Include lodgings. Default is false
			//"unavailable"   => true, #Include unavailable properties. Default is false
			"description" => true, #Get property description. Default is false.
			"thumbnails" => true, #Get property image 580width and 75width. Default is false. CAUTION: The requests might be loading slower!
			//"page" => 2, #Default is 1
			"pagelen" => 10, #Default is 20
			//"sort" => 3, #Sort by: 1=Default, 2=Most Recent, 3=Lowest price, 4=Highest Price, 5=Latest updated. Default is 1.
			"showProperties" => true, #Version 1.1 = Show properties in the search. Default is true
			"showStudentHalls" => true, #Version 1.1 = Show student halls in the search. Default is false
		);
	
	print_r($st->call("search", $params));
	echo "\n===========================\n\n";
	
	echo "=========================== PROPERTY VIEW\n"; //ID is Required
	print_r($st->call("property", array(
			"id"=>394,			
			#"thumbnails" => true, #Get property image 580width and 75width. Default is false. CAUTION: The requests might be loading slower!
		)));
	echo "\n===========================\n\n";*/
	
	echo "=========================== HALL VIEW - VERSION 1.1\n"; //ID is Required
	print_r($st->call("hall", array(
			"id"=>1
		)));
	echo "\n===========================\n\n";

	/*echo "=========================== Request Information\n"; //property,username,email,message are required
	$params = array(
			//"property" => 394, #Property ID
			"hall"	   => 9, #Hall ID. Version 1.1. WARNING: From version 1.1 you can EITHER specify a property id OR a hall id. If you specify both, you will get an error, if you ommit both of them, you will get an error.
			"username" => "TCR", #The user's full name. Please advise users to use their legal name
			"email"    => "romeo@jakuta.co.uk", #The user's email. Our system will check if that is valid and if it's not a temp email. Please take measures as well.
			"phone"    => "07858542799", #The user's phone number. Altough they are not required please advise users to add their phone number. Also, if it's an international number don't forget to add the international prefix
			"group"    => 1, #How many people are interested in this property. If the user is a lead tenant on behalf of a group, please advise your users to specify the group size
			"message"  => "This is a test from the API" #This is the message that your users wants to send to the landlord. Please advise your users to write messages in english our provide some kind of translation before that.
		);
	print_r($st->call("requestInfo", $params));
	echo "\n===========================\n\n";
	
	echo "=========================== GET TOWN LIST\n";
	print_r($st->call("towns"));
	echo "\n===========================\n\n";*/
?>