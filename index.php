<?php
	ob_start();
	set_time_limit(0);
	include "system/studenttenant.sdk.php";
	include "system/config.php";
	
	$st = new StudentTenant($_STConfig);
	
	include "system/functions.php";
	
	switch (@$_GET['page']){
		case "search": $page = "search"; break;
		case "property": $page = "property"; break;
		case "hall": $page = "hall"; break;
		case "requestinfo": $page = "requestinfo"; break;
		case "documentation": $page = "documentation"; break;
		default:
			$page = "index";
	}

	include "pages/_header.php";
		include "pages/".$page.".php";
	include "pages/_footer.php";
?>
  