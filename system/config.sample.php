<?php
	# Please rename this file to config.php and add your Authentication Details
	$_STConfig = array(
			"client"	=> "StudentTenant_Client", 
			"token"		=> "StudentTenant_Token",
			"sandbox"	=> true,
			"API_URL"	=> "https://www.studenttenant.com/api"
		);
		
	/* 
		Environment Selection
		
		dev  - Will display all the errors as well as what parameters were send and what parameters were not sent
		prod - Will display error pages instead of errors (demo of Exception overwrite rule) as well as hide all the parameters that were sent/received 
	*/
	define("env", "prod");
	
	/* 
		Image Caching Selection
		
		0 - The images will not be cached, the original will be displayed instead
		1 - The images will be cached to the server via PHP
		
		Note: In the future another option will be added, << 2 >> which will provide caching using AJAX and PHP.
			  This will allow caching images without making the website load slower the first time the image is loaded
			  DO NOT USE 2. This note will disappear once this feature will be enabled and you will be able to see above, in the options list
	*/
	define("IMAGE_CACHING", 0);
?>