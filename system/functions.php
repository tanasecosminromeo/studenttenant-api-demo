<?php
	include_once "system/resize.class.php";

	# Load 10 Random Properties
	function load10RandomProperties(){ global $st;
		$randTown = array("gillingham", "canterbury", "rochester", "farnham");
		$randTown = $randTown[rand(0, sizeof($randTown)-1)];
		$params = array(
				//"location" => "Church Street", #Default is null. Can be a street name or a postcode. Not case sensitive
				"town"     => $randTown, #Default is null. Not case sensitive
				//"maxRent"  => 390, #Rent per person per month. Default is 0	
				//"minRent"  => 300, #Rent per person per month. Default is 99999
				"images"   => true, #Show only properties with images. Default false
				//"bedrooms" => 0, #Default show all
				//"virtualtour" => true, #Show only properties with virtual tours. Default is false
				//"accredited"  => true, #Show only accredited properties. Default is false
				//"lodgings"    => true, #Include lodgings. Default is false
				//"unavailable"   => true, #Include unavailable properties. Default is false
				"description" => true, #Get property description. Default is false.
				//"page" => 2, #Default is 1
				"pagelen" => 10, #Default is 20
				"sort" => 3, #Sort by: 1=Default, 2=Most Recent, 3=Lowest price, 4=Highest Price, 5=Latest updated. Default is 1.
			);
		
		$randProperties = $st->call("search", $params);
		return $randProperties['properties'];
	}
	
	# Offer Box
	function offer_box($data, $class="offer_box"){
	  $link  = 'index.php?page=property&id='.$data['id'];
	  $image = @$data['images'][0]['original'];
	  if (!strstr($image, "http")){ $image = "images/no-photo-130.jpg"; }
	  echo '<div class="'.$class.'"> <a href="'.$link.'"><img src="'.image($image, 103, 98).'" width="130" height="98" class="img_left" alt="" border="0" /></a>
        <div class="offer_info"> <span>'.$data['address']['street'].', '.$data['address']['town'].'<br /><small style="color: #111;">From &pound;'.intval($data['rent']).'pppm / '.intval($data['bedrooms']).' bedrooms</small></span>
          <p class="offer"> &quot;'.substr($data['description'], 0, 100).'...&quot; </p>
          <div class="more"><a href="'.$link.'">more >></a></div>
        </div>
      </div>';
	}
	
	# Offer Box Halls
	function offer_box_hall($data, $class="offer_box"){
	  $link  = 'index.php?page=hall&id='.$data['id'];
	  $image = @$data['images'][0]['original'];
	  if (!strstr($image, "http")){ $image = "images/no-photo-130.jpg"; }
	  echo '<div class="'.$class.' '.$class.'_hall"> <a href="'.$link.'"><img src="'.image($image, 130, 98).'" width="130" height="98" class="img_left" alt="" border="0" /></a>
        <div class="offer_info"> <span>'.$data['name'].'<br /><small style="color: #111;">From &pound;'.intval($data['rent']).'pppm / '.intval(sizeof($data['rooms'])).' room types</small></span>
          <p class="offer"> &quot;'.substr($data['description'], 0, 100).'...&quot; </p>
          <div class="more"><a href="'.$link.'">more >></a></div>
        </div>
      </div>';
	}
	
	# Error handler
	function stErrorHandler($errno, $errstr, $errfile, $errline){
		if (!(error_reporting() & $errno)) { return; }

		switch ($errno) {
			case E_USER_ERROR:
				$message = "Fatal user error on line <strong>".$errline."</strong> in file <strong>".$errfile."</strong>.<br />".$errstr;
				$errno   = "901";
				$tag     = "User error";
				break;

			case E_USER_WARNING:
				$message = "User warning on line <strong>".$errline."</strong> in file <strong>".$errfile."</strong>.<br />".$errstr;
				$errno   = "902";
				$tag     = "User warning";
				break;

			case E_USER_NOTICE:
				$message = "User notice on line <strong>".$errline."</strong> in file <strong>".$errfile."</strong>.<br />".$errstr;
				$errno   = "903";
				$tag     = "User notice";
				break;

			default:
				$message = "Unknown error type on line <strong>".$errline."</strong> in file <strong>".$errfile."</strong>.<br />".$errstr;
				$errno   = "900";
				$tag     = "Unknown error type";
				break;
		}

		ob_end_clean();
		$html    = file_get_contents("error.htm");
		print str_replace(array("[ERROR-MESSAGE]", "[ERROR-NUMBER]", "[ERROR-TAG]"), array($message, $errno, $tag), $html);
		exit;
		
		return true;
	}
	
	function stExceptionHandler($exception) {
		ob_end_clean();
		
		$em 	 = $exception->getMessage();
		if ($em=="Server is upgrading please try again."){
			print file_get_contents("upgrading.htm");
		} else {		
			if (strstr($em, ":")){
				$ex       = explode(':', $em, 2);		
				$message  = "Uncaught exception: ".$ex[1];
				$errorno  = str_replace("Error ", "", $ex[0]);
			} else {
				$message  = $em;
				$errorno  = "999";
			}
			
			$html    = file_get_contents("error.htm");
			
			print str_replace(array("[ERROR-MESSAGE]", "[ERROR-NUMBER]", "[ERROR-TAG]"), array($message, $errorno, "Uncaught exception"), $html);
		}
		
		exit;
	}
	
	if (env=="prod"){ //Set custom errors & exceptions for production environment 
		set_error_handler("stErrorHandler");
		set_exception_handler("stExceptionHandler");
	}
	
	function image($url, $sizeW="original", $sizeH="original", $IMAGE_CACHING=IMAGE_CACHING){
		if ($IMAGE_CACHING==0){
			return $url;
		}
		
		if (strstr($url, "no-photo.jpg")){ return "/images/no-photo.jpg"; }
		$url 	   = str_replace("https://", "http://", $url);
		$base      = "cache/images/";		
		$tag	   = strlen($url).sha1($url).".jpg";
		
		if ($sizeW=="original" && $sizeH=="original"){
			$size = "original";
		} else {
			$size = $sizeW."X".$sizeH;
		}
		
		if (!file_exists($base."original/".$tag)){
			if (!is_dir($base."original")){ mkdir($base."original"); }
			$data = @file_get_contents($url);
			
			if (strlen($data)<100){ return "/images/no-photo.jpg"; }
			file_put_contents($base."original/".$tag, $data);
		}
		
		if ($size!="original"){
			if (!is_dir($base.$size)){ mkdir($base.$size); }
	
			if (!file_exists($base.$size."/".$tag)){
				$resizeObj = new resize($base."original/".$tag);
				
				if (!$resizeObj->hasError()){				
					if ($sizeW=="original"){
						$resizeObj -> resizeImage($sizeW, 200, 'landscape');
					} elseif ($sizeH=="original") {
						$resizeObj -> resizeImage(200, $sizeH, 'portrait');
					} else {
						$resizeObj -> resizeImage($sizeW, $sizeH, 'exact');
					}
					
					$resizeObj -> saveImage($base.$size."/".$tag, 94);
				} else {
					return "/images/no-photo.jpg";
				}
			}
		}
		
		return "/".$base.$size."/".$tag;
	}
?>