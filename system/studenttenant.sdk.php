<?php
class StudentTenant {

	const SDK_VERSION = 'ST/PHPSDK/v1.3';

	private $client;
	private $token;
	private $sandbox;
	private $API_URL;
	private $ssl_version;
	
	public function __construct($config) {
		if (empty($config['client']))
			throw new Exception('$config[\'client\'] parameter cannot be empty');
		if (empty($config['token']))
			throw new Exception('$config[\'token\'] parameter cannot be empty');
		$this->client = $config['client'];
		$this->token  = $config['token'];
		
		if (!isset($config['sandbox'])){ 	$this->sandbox = true; } else { $this->sandbox = $config['sandbox']; }
		if (empty($config['API_URL'])){ 	$this->API_URL = "https://www.studenttenant.com/api"; } else { $this->API_URL = $config['API_URL']; }
		if (empty($config['ssl_version'])){ $this->ssl_version = 3; } else { $this->ssl_version = $config['ssl_version']; }
	}
	
	public function call($action, $params=array()){
		$fields = array_merge($params, array('client' => $this->client, 'token' => $this->token, 'action' => $action, 'ip' => $_SERVER['REMOTE_ADDR']));
						
		$fields_string = "";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->API_URL);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSLVERSION, $this->ssl_version); 
		if ($this->sandbox){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}
		$result = curl_exec($ch);
		$error  = @curl_error($ch);
		
		curl_close($ch);
		
		$array = @json_decode($result, true);
		if (!isset($array['error'])){
			if (strstr($result, '/upgrading/')){
				throw new Exception('Server is upgrading please try again.');
			} else {
				if ($this->sandbox){ //Return sent params
					$data_send = ' DATA Sent: ['.$fields_string.']';
				} else { $data_send = ''; }
				
				throw new Exception('Error F-IR-00: ['.$result.']. cURL Error: ['.$error.'].'.$data_send);
			}
		} else {
			if (isset($array['errorid'])){
				throw new Exception('Error '.$array['errorid'].': '.$array['message']);
			} else {
				return $array;
			}
		}
	}
	
}
?>