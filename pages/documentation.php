<div id="main_content">
	<?php include "_sidebar.php"; ?>
	<div class="column4">
	<div class="title">Documentation</div>
		<p>&nbsp;</p>
		<p><strong>API Version: </strong> <?php $v = $st->call("version"); print $v['version']; ?></p>
		<p><strong>DEMOSITE Version: </strong> 1.12-STABLE</p>
		<p>&nbsp;</p>
		<p style="color: #FF0000; font-size: 18px; text-align: center;" align="center">Please use the provided source code of the DEMO Website for reference.<br /><br />Feel free to contact our technical support at any time!</p>
		<p>&nbsp;</p>
		<p><strong>Changes Version Codes</strong><br />New parameters / functions will be marked with a different color to make them easier to spot, as follows:
				<ul>
					<li class="v112">From v0.1-ALPHA -> v1.12-STABLE</li>
				</ul>
			</p>
		<p>&nbsp;</p>
		<p>
			<h2>1. Introduction</h2><br />
			StudentTenant API allows connecting to the StudentTenant Property Database.<br />
			All the parameters sent to the API must be sent VIA POST and the response will always be in JSON format.<br />
			When encountering an error you will get the following parameters:<br />
				<ul>
					<li><strong>message</strong>: A detailed message of what the error is</li>
					<li><strong>errorid</strong>: An error ID which will help Jakuta Ltd. assist you</li>
					<li><strong>error</strong>: A Boolean parameter returning True.
				</ul>
			<em>Sample: {"message":"Client name not specified","errorid":"F-ND-01","error":true}</em><br /><br />
			<strong>Important note: </strong>The error parameter will always be sent either <strong>True</strong> or <strong>False</strong> and it will help your software figure out if the response is as expected or an error needs to shop up.
			<br /><strong>Configuration: </strong>
			To use the API you will need a valid <strong>client</strong> and <strong>token</strong> provided by StudentTenant. Please store that safely.<br />
			To enhance security please use <strong>HTTPS</strong>. Our server is configured using <strong>SSL3</strong>, make sure you configure your software correctly.<br /><br />
			<center>
				<em>PHP Example with cURL</em><br />
				curl_setopt($ch, CURLOPT_SSLVERSION, 3); <br /><br />
			</center>
		</p>
		<p>
			<h2>2. Making a request</h2><br />
			If you are using a software built in PHP we recommend you use our provided PHP SDK. If you do not use PHP or you do not want to use the SDK please follow the guidelines below.<br /><br />
			Always sent <strong>POST</strong> requests to the API URL <em>(https://www.studenttenant.com/api)</em>. The following parameters are required:
				<ul>
					<li><strong>client</strong>: your client name</li>
					<li><strong>token</strong>: your authentication token / password</li>
					<li><strong>action</strong>: the action/call name for the api <em>(eg: 'version', 'search', 'property', 'requestInfo', 'towns')</em></li>
					<li>any parameters required by a specific action <em>(for more info check the howto of each action)</em></li>
					<li><strong>ip</strong>: the IP of the user that is using your software<br />
						We need this to store analytics details about who views what property as well as detecting abuses!!!<br />
						If you do not provide a unique, REAL IP for all your users and one of them commits and abuse we may disable your account instead of disabling access of that IP.
					</li>
				</ul>
		</p>
		<p>
			<h2>3.1 Get version - [action] = "version"</h2><br />
			Curious of what is our current API version? Set the action parameter to "version" with no other required parameters and you will get the version number.<br /><br />
			<center>
				<em>Params Sent</em><br />
				Array ( [client] => CLIENT , [token] => TOKEN, [action] => "version", [ip] => USER_IP )<br /><br />
				<em>Request Returned</em><br />
				{"version":"ST/API/0.1-ALFA","error":false}<br /><br />
				<em>JSON->Array Returned</em><br />
				Array ( [version] => ST/API/0.1-ALFA [error] => false )
			</center>
		</p>
		<p>
			<h2>3.2 Search for properties - [action] = "search"</h2><br />
			The most important function of the API will be to search for properties<span class="v112">/student halls</span>. No parameters are required, however the list of optional parameters is long and vital, please read it carefully.<br /><br />
			
			<h3>3.2.1 Create a new search - [action] = "search"</h3><br />
				<center><br /><em>Params Sent</em></center>
				
				<ul>					
					<li style="color: #002a8a"><small><strong>client</strong>: your client name</small></li>
					<li style="color: #002a8a"><small><strong>token</strong>: your authentication token / password</small></li>
					<li style="color: #002a8a"><small><strong>action</strong>: the action/call name for the api <em>(eg: 'version', 'search', 'property', 'requestInfo', 'towns')</em></small></li>
					<li style="color: #002a8a"><small><strong>ip</strong>: the IP of the user that is using your software<br />
						We need this to store analytics details about who views what property as well as detecting abuses!!!<br />
						If you do not provide a unique, REAL IP for all your users and one of them commits and abuse we may disable your account instead of disabling access of that IP.
					</small></li>
					<li><strong>bedrooms</strong>: Integer. Default=0. Send 0 if you want to get all properties or a valid integer such as 1,2,3,4,5,etc to see just properties with 1 bedroom, 2 bedrooms, 3 bedrooms, etc</li>
					<li><strong>location</strong>: String. Default=Null. Can be a street name or a postcode. Not case sensitive</li>
					<li><strong>town</strong>: String. Default=Null. Not case sensitive</li>
					<li><strong>minRent</strong>: Integer. Default=0. This is the minimum rent per person per month in GBP that a property should have</li>
					<li><strong>maxRent</strong>: Integer. Default=99999. This is the maximum rent per person per month in GBP that a property should have</li>
					<li><strong>images</strong>: Boolean. Default=false. Set this to true if you want to return just properties with images</li>
					<li><strong>thumbnails</strong>: Boolean. Default=false. If this is set to true, along with the original images you will get links of thumbnails with 580px width and 75px width. Please be aware that this might slow down your request significantly <em>(in specific cases)</em> and that it is advisable to provide your own method to create thumbnails.</li>
					<li><strong>virtualtour</strong>: Boolean. Default=false. Set this to true if you want to return just properties with virtual tours</li>
					<li><strong>accredited</strong>: Boolean. Default=false. Set this to true if you want to return just properties that are accredited</li>
					<li><strong>lodgings</strong>: Boolean. Default=false. If this is true lodgings will be included in the results</li>
					<li><strong>unavailable</strong>: Boolean. Default=false. If this is true unavailable properties will be included in the results</li>
					<li><strong>description</strong>: Boolean. Default=false. If this is true you will get the description of each property. This can be useful when displaying the results, although it will slightly slow down the request</li>
					<li><strong>page</strong>: Integer. Default=1. This will be the page number of the request. It will return an error if the page number is greater than the maximum page number <em>(totalresults/pagelen)</em></li>
					<li><strong>pagelen</strong>: Integer. Default=10. This represents how many properties should be returned per page</li>
					<li><strong>sort</strong>: Integer. Default=1.
							<ul>
								<li>1=Default</li>
								<li>2=Most Recent</li>
								<li>3=Lowest price</li>
								<li>4=Highest Price</li>
								<li>5=Latest updated</li>
							</ul>					
						</li>
					<li class="v112">v1.1: <strong>showProperties</strong>: Boolean. Default=true. Show properties in the search.</li>
					<li class="v112">v1.1: <strong>showStudentHalls</strong>: Boolean. Default=false <em>(to preserve backed compatibility)</em>. Show student halls in the search</li>
				</ul>
			<br /><br />
			<center>
				<em>Response Structure [JSON to Array]</em><br />
			</center>
				Array<br />
					(<br />
						<span class="tab"></span>[<strong>searchid</strong>] => This search ID<br />
						<span class="tab"></span>[<strong>page</strong>] => Page Number<br />
						<span class="tab"></span>[<strong>pagelen</strong>] =>Page Length<br />
						<span class="tab"></span>[<strong>total</strong>] => Total Results. Eg: 310 properties were found, only 10 are returned in the properties array. To view the rest you need to make the same request with page=2,3..(310/10=31).<br />
						<span class="tab"></span>[<strong>properties</strong>] => Array<br />
							<span class="tab"></span>(<br />
								<span class="tab2"></span>[<strong>0</strong>] => Array (<br />
									<span class="tab3"></span>[<strong>id</strong>] => Property ID on StudentTenant<br />
									<span class="tab3"></span>[<strong>address</strong>] => Array<br />
										<span class="tab3"></span>(<br />
											<span class="tab4"></span>[<strong>number</strong>] => Street Number<br />
											<span class="tab4"></span>[<strong>street</strong>] => Street Name<br />
											<span class="tab4"></span>[<strong>town</strong>] => Town<br />
											<span class="tab4"></span>[<strong>county</strong>] => County<br />
											<span class="tab4"></span>[<strong>postcode</strong>] => Postcode<br />
										<span class="tab3"></span>) <em>//endOf address array</em><br />
									<span class="tab3"></span>[<strong>rent</strong>] => Minimum rent per person per month<br />
									<span class="tab3"></span>[<strong>bedrooms</strong>] => Number of bedrooms<br />
									<span class="tab3"></span>[<strong>available</strong>] => Number of bedrooms available<br />
									<span class="tab3"></span>[<strong>accredited</strong>] => True if the property is accredited or False if it's not<br />
									<span class="tab3"></span>[<strong>images</strong>] => Array<br />
										<span class="tab3"></span>(<br />
											<span class="tab4"></span>[<strong>0</strong>] => Array<br />
												<span class="tab4"></span>(<br />
													<span class="tab5"></span>[<strong>original</strong>] => LINK to original Image 1<br />
													<span class="tab5"></span>[<strong>img580</strong>] => LINK to 580px width thumbnail image 1 - It will return only if the thumbnails param is set to True<br />
													<span class="tab5"></span>[<strong>img75</strong>] => LINK to 75px width thumbnail image 1 - It will return only if the thumbnails param is set to True<br />
												<span class="tab4"></span>)<br />
										<span class="tab3"></span>) <em>//endOf images array</em><br />
									<span class="tab3"></span>[<strong>type</strong>] => Property Type (House/Flat/Studio/etc)<br />
									<span class="tab3"></span>[<strong>description</strong>] => Property Description - It will return only if the description param is set to True<br />
								<span class="tab2"></span>)<br />
								<span class="tab2"></span><em>--------------------------------------------------------------------------------------</em><br />
								<span class="tab2"></span><em>--Other properties. Array ID from 0 to pagelen-1<span class="v112">-sizeof($halls)</span>--</em><br />
								<span class="tab2"></span><em>--------------------------------------------------------------------------------------</em><br />
							<span class="tab"></span>) <em>//endOf properties array</em><br />
						<span class="v112">
						<span class="tab"></span>v1.1: [<strong>halls</strong>] => Array<br />
							<span class="tab"></span>(<br />
								<span class="tab2"></span>[<strong>0</strong>] => Array (<br />
									<span class="tab3"></span>[<strong>id</strong>] => Hall ID on StudentTenant<br />
									<span class="tab3"></span>[<strong>name</strong>] => Name of the student hall<br />
									<span class="tab3"></span>[<strong>address</strong>] => Array<br />
										<span class="tab3"></span>(<br />
											<span class="tab4"></span>[<strong>number</strong>] => Street Number<br />
											<span class="tab4"></span>[<strong>street</strong>] => Street Name<br />
											<span class="tab4"></span>[<strong>town</strong>] => Town<br />
											<span class="tab4"></span>[<strong>county</strong>] => County<br />
											<span class="tab4"></span>[<strong>postcode</strong>] => Postcode<br />
										<span class="tab3"></span>) <em>//endOf address array</em><br />
									<span class="tab3"></span>[<strong>rent</strong>] => Minimum rent per person per month<br />
									<span class="tab3"></span>[<strong>rooms</strong>] => Array<br />
										<span class="tab3"></span>(<br />
											<span class="tab4"></span>[<strong>name</strong>] => Name of room 1<br />
											<span class="tab4"></span>[<strong>rent</strong>] => Rent per person per month for room 1<br />
										<span class="tab3"></span>) <em>//endOf rooms array</em><br />
									<span class="tab3"></span>[<strong>images</strong>] => Array<br />
										<span class="tab3"></span>(<br />
											<span class="tab4"></span>[<strong>0</strong>] => Array<br />
												<span class="tab4"></span>(<br />
													<span class="tab5"></span>[<strong>original</strong>] => LINK to original Image 1<br />
													<span class="tab5"></span><em>Halls will never return img580 and img75, no matter if thumbnails param is set to True or False</em><br />
												<span class="tab4"></span>)<br />
										<span class="tab3"></span>) <em>//endOf images array</em><br />
									<span class="tab3"></span>[<strong>description</strong>] => Student hall Description - It will return only if the description param is set to True<br />
								<span class="tab2"></span>)<br />
								<span class="tab2"></span><em>--------------------------------------------------------------------------------------</em><br />
								<span class="tab2"></span><em>--Other halls. Array ID from 0 to pagelen-1-sizeof($properties)--</em><br />
								<span class="tab2"></span><em>--------------------------------------------------------------------------------------</em><br />
							<span class="tab"></span>) <em>//endOf halls array</em><br />
						</span>
						<span class="tab"></span>[<strong>searchParams</strong>] => Array <em>Useful to recreate/amend the search</em><br />
							<span class="tab"></span>(<br />
								<span class="tab2"></span>[<strong>bedrooms</strong>] => Number of bedrooms you searched for<br />
								<span class="tab2"></span>[<strong>location</strong>] => Location where you searched<br />
								<span class="tab2"></span>[<strong>town</strong>] => Town where you searched<br />
								<span class="tab2"></span>[<strong>maxRent</strong>] => Maximum Rent you searched for<br />
								<span class="tab2"></span>[<strong>minRent</strong>] => Minimum Rent you searched for<br />
								<span class="tab2"></span>[<strong>images</strong>] => True/False<br />
								<span class="tab2"></span>[<strong>virtualtour</strong>] => True/False<br />
								<span class="tab2"></span>[<strong>accredited</strong>] => True/False<br />
								<span class="tab2"></span>[<strong>lodgings</strong>] => True=Include Lodgings/False=Exclude Lodgings<br />
								<span class="tab2"></span>[<strong>unavailable</strong>] => True=Include Unavailable/False=Exclude Unavailable<br />
								<span class="tab2"></span>[<strong>description</strong>] => True=Include description parameter/False=Exclude description parameter<br />
								<span class="tab2"></span>[<strong>page</strong>] => Page number you asked for<br />
								<span class="tab2"></span>[<strong>pagelen</strong>] => Page length you asked for<br />
								<span class="tab2"></span>[<strong>sort</strong>] => Sorting type you asked for<br />
								<span class="v112"><span class="tab2"></span>[v1.1: <strong>showProperties</strong>] => True/False</span><br />
								<span class="v112"><span class="tab2"></span>[v1.1: <strong>showHalls</strong>] => True/False</span><br />
							<span class="tab"></span>)<em>//endOf searchParams array</em><br />
						<span class="tab"></span>[<strong>error</strong>] => False <em>If True the above params will not be returned</em><br />
				)//endOf response array<br /><br />
				
			<h3>3.2.2 View other page / Amend old search - [action] = "search"</h3><br />
				You can easily amend or see another page from an existing search. To do this you just have to send a valid searchid as a parameter. Please find below the parameters that you can send for this.<br />
				<center><br /><em>Params Sent</em></center>
				<ul>
					<li style="color: #002a8a"><small><strong>client</strong>: your client name</small></li>
					<li style="color: #002a8a"><small><strong>token</strong>: your authentication token / password</small></li>
					<li style="color: #002a8a"><small><strong>action</strong>: the action/call name for the api <em>(eg: 'version', 'search', 'property', 'requestInfo', 'towns')</em></small></li>
					<li style="color: #002a8a"><small><strong>ip</strong>: the IP of the user that is using your software<br />
						We need this to store analytics details about who views what property as well as detecting abuses!!!<br />
						If you do not provide a unique, REAL IP for all your users and one of them commits and abuse we may disable your account instead of disabling access of that IP.
					</small></li>
					<li><strong>searchid</strong>: Integer. REQUIRED. This represents the ID of an existing search. You can provide only IDs returned by the API. In case you return an invalid id you will get an error.</li>
					<li><strong>page</strong>: Integer. Default=1. This will be the page number of the request. It will return an error if the page number is greater than the maximum page number <em>(totalresults/pagelen)</em></li>
					<li><strong>pagelen</strong>: Integer. Default=10. This represents how many properties should be returned per page</li>
					<li><strong>sort</strong>: Integer. Default=1.
							<ul>
								<li>1=Default</li>
								<li>2=Most Recent</li>
								<li>3=Lowest price</li>
								<li>4=Highest Price</li>
								<li>5=Latest updated</li>
							</ul>					
						</li>
				</ul>
			<br /><br />
			<center>
				<em>The return response is in the same format as before</em><br />
			</center>
		</p>
		<p>
			<h2>3.3.1 Get property data - [action] = "property"</h2><br />
			This will return all the information for a property reference by the ID.<br />
			<center><br /><em>Params Sent</em></center>
				<ul>
					<li style="color: #002a8a"><small><strong>client</strong>: your client name</small></li>
					<li style="color: #002a8a"><small><strong>token</strong>: your authentication token / password</small></li>
					<li style="color: #002a8a"><small><strong>action</strong>: the action/call name for the api <em>(eg: 'version', 'search', 'property', 'requestInfo', 'towns')</em></small></li>
					<li style="color: #002a8a"><small><strong>ip</strong>: the IP of the user that is using your software<br />
						We need this to store analytics details about who views what property as well as detecting abuses!!!<br />
						If you do not provide a unique, REAL IP for all your users and one of them commits and abuse we may disable your account instead of disabling access of that IP.
					</small></li>
					<li><strong>id</strong>: Integer. REQUIRED. This is the property ID that you want the details for.</li>
					<li><strong>thumbnails</strong>: Boolean. Default=false. If this is set to true, along with the original images you will get links of thumbnails with 580px width and 75px width. Please be aware that this might slow down your request significantly <em>(in specific cases)</em> and that it is advisable to provide your own method to create thumbnails.</li>
				</ul>
			<br /><br />
			<center>
				<em>Response Structure [JSON => Array]</em><br />
			</center>
			<span class="tab"></span>Array<br />
				<span class="tab2"></span>(<br />
					<span class="tab3"></span>[<strong>id</strong>] => Integer. Property Id<br />
					<span class="tab3"></span>[<strong>address</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>number</strong>] => String. Street Number (can contain letters, eg: 12A)<br />
							<span class="tab5"></span>[<strong>street</strong>] => String. Street Name<br />
							<span class="tab5"></span>[<strong>town</strong>] => String. Town<br />
							<span class="tab5"></span>[<strong>county</strong>] => String. County<br />
							<span class="tab5"></span>[<strong>postcode</strong>] => String. Postcode<br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>rent</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>lowest</strong>] => Float. Minimum rent per person per month<br />
							<span class="tab5"></span>[<strong>total</strong>] => Float. Total rent for the whole property<br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>bedrooms</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>0</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>id</strong>] => Integer. StudentTenant Bedroom ID<br />
									<span class="tab7"></span>[<strong>name</strong>] => String. Bedroom Name (usually it's bedroom 1, 2, etc...)<br />
									<span class="tab7"></span>[<strong>rent</strong>] => Float. Rent per person per month<br />
									<span class="tab7"></span>[<strong>ensuite</strong>] => Boolean. True if the bedroom has an en-suite/False if it doesn't<br />
									<span class="tab7"></span>[<strong>ensuite</strong>] => Boolean. True if the bedroom is available/False if it is unavailable<br />
								<span class="tab6"></span>)<br />

							<span class="tab5"></span>[<strong>1</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>id</strong>] => Integer. StudentTenant Bedroom ID<br />
									<span class="tab7"></span>[<strong>name</strong>] => String. Bedroom Name (usually it's bedroom 1, 2, etc...)<br />
									<span class="tab7"></span>[<strong>rent</strong>] => Float. Rent per person per month<br />
									<span class="tab7"></span>[<strong>ensuite</strong>] => Boolean. True if the bedroom has an en-suite/False if it doesn't<br />
									<span class="tab7"></span>[<strong>ensuite</strong>] => Boolean. True if the bedroom is available/False if it is unavailable<br />
								<span class="tab6"></span>)<br />

							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />
							<span class="tab5"></span><em>--------- All bedrooms will be listed in the same format ----------</em><br />
							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />

						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>fees</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>administrationFee</strong>] => Float. The administration fee the owner/agent of the property charge &pounds; per person<br />
							<span class="tab5"></span>[<strong>deposit</strong>] => String. The deposit the owner/agent of the property charge Might be a range, if currency not specified => GBP<br />
							<span class="tab5"></span>[<strong>otherFee</strong>] => Float. Other fees quantum that the owner/agent of the property might charge &pounds; per person<br />
							<span class="tab5"></span>[<strong>referencingFee</strong>] => Float. The StudentTenant referencing fee for this property &pounds; per person<br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>epc</strong>] => Array - This array will be empty in case no EPC info is sent<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>number</strong>] => String. EPC Number (eg: 9658-3086-6237-9262-4930)<br />
							<span class="tab5"></span>[<strong>energyefficiency_actual</strong>] => String, 1 char(A,B,C,D,E,F,G). Energy Efficiency Actual Grade<br />
							<span class="tab5"></span>[<strong>energyefficiency_possible</strong>] => String, 1 char(A,B,C,D,E,F,G). Energy Efficiency Possible Grade<br />
							<span class="tab5"></span>[<strong>environmentalimpact_actual</strong>] => String, 1 char(A,B,C,D,E,F,G). Environmental Impact Actual Grade<br />
							<span class="tab5"></span>[<strong>environmentalimpact_possible</strong>] => String, 1 char(A,B,C,D,E,F,G). Environmental Impact Possible Grade<br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>features</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>0</strong>] => String. Feature X<br />
							<span class="tab5"></span>[<strong>1</strong>] => String. Feature Y<br />

							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />
							<span class="tab5"></span><em>----------- All features will be listed in the same format ------------</em><br />
							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />
							
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>available</strong>] => Integer. Number of bedrooms the property has<br />
					<span class="tab3"></span>[<strong>accredited</strong>] => Boolean. True if the property is accredited/False if it's not<br />
					<span class="tab3"></span>[<strong>images</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>0</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>original</strong>] => String, URL of "Property Image 1"<br />
								<span class="tab6"></span>)<br />
							<span class="tab5"></span>[<strong>1</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>original</strong>] => String, URL of "Property Image 2"<br />
								<span class="tab6"></span>)<br />
							<span class="tab5"></span>[<strong>2</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>original</strong>] => String, URL of "Property Image 3"<br />
								<span class="tab6"></span>)<br />
							
							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />
							<span class="tab5"></span><em>----------- All images will be listed in the same format ------------</em><br />
							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />

						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>description</strong>] => String. "Property Description"<br />
					<span class="tab3"></span>[<strong>stations</strong>] => Array - This array will display the closest train stations<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>String. Station X Name<span class="v112">||Integer. 0=Train/1=Underground/2=Both</span></strong>] => Integer. Distance in miles from the property<br />
							<span class="tab5"></span>[<strong>String. Station Y Name<span class="v112">||Integer. 0=Train/1=Underground/2=Both</span></strong>] => Integer. Distance in miles from the property<br />
						<span class="tab4"></span>)<span class="tab"></span><br />

					<span class="tab3"></span>[<strong>error</strong>] => False <em>If True the above params will not be returned</em><br />
				<span class="tab2"></span>)<br />
			
			<br />
			
			<center>
				<em>Example Request</em><br />
			</center>
			<span class="tab"></span>Array<br />
				<span class="tab2"></span>(<br />
					<small><span class="tab3"></span>[<strong>action</strong>] => "property"</small><br />
					<small><span class="tab3"></span>[<strong>ip</strong>] => "127.0.0.1"</small><br />
					<small><span class="tab3"></span>[<strong>client</strong>] => "demo"</small><br />
					<small><span class="tab3"></span>[<strong>token</strong>] => "token"</small><br />
					<span class="tab3"></span>[<strong>id</strong>] => 394<br />
					<span class="tab3"></span>[<strong>thumbnails</strong>] => False<br />
				<span class="tab2"></span>)<br /><br />
				
			<center>
				<em>Example Response</em><br />
			</center>
			<span class="tab"></span>Array<br />
				<span class="tab2"></span>(<br />
					<span class="tab3"></span>[<strong>id</strong>] => 394<br />
					<span class="tab3"></span>[<strong>address</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>number</strong>] => 33<br />
							<span class="tab5"></span>[<strong>street</strong>] => Church Street<br />
							<span class="tab5"></span>[<strong>town</strong>] => Gillingham<br />
							<span class="tab5"></span>[<strong>county</strong>] => Kent<br />
							<span class="tab5"></span>[<strong>postcode</strong>] => ME7 1SR<br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>rent</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>lowest</strong>] => 330.00<br />
							<span class="tab5"></span>[<strong>total</strong>] => 1410<br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>bedrooms</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>0</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>id</strong>] => 1799<br />
									<span class="tab7"></span>[<strong>name</strong>] => Bedroom 1<br />
									<span class="tab7"></span>[<strong>rent</strong>] => 330.00<br />
									<span class="tab7"></span>[<strong>ensuite</strong>] => <br />
									<span class="tab7"></span>[<strong>available</strong>] => 1<br />
								<span class="tab6"></span>)<br />

							<span class="tab5"></span>[<strong>1</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>id</strong>] => 1800<br />
									<span class="tab7"></span>[<strong>name</strong>] => Bedroom 2<br />
									<span class="tab7"></span>[<strong>rent</strong>] => 360.00<br />
									<span class="tab7"></span>[<strong>ensuite</strong>] => <br />
									<span class="tab7"></span>[<strong>available</strong>] => 1<br />
								<span class="tab6"></span>)<br />

							<span class="tab5"></span>[<strong>2</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>id</strong>] => 1801<br />
									<span class="tab7"></span>[<strong>name</strong>] => Bedroom 3<br />
									<span class="tab7"></span>[<strong>rent</strong>] => 360.00<br />
									<span class="tab7"></span>[<strong>ensuite</strong>] => <br />
									<span class="tab7"></span>[<strong>available</strong>] => 1<br />
								<span class="tab6"></span>)<br />

							<span class="tab5"></span>[<strong>3</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>id</strong>] => 1802<br />
									<span class="tab7"></span>[<strong>name</strong>] => Bedroom 4<br />
									<span class="tab7"></span>[<strong>rent</strong>] => 360.00<br />
									<span class="tab7"></span>[<strong>ensuite</strong>] => <br />
									<span class="tab7"></span>[<strong>available</strong>] => 1<br />
								<span class="tab6"></span>)<br />

						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>fees</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>administrationFee</strong>] => 160.00<br />
							<span class="tab5"></span>[<strong>deposit</strong>] => 0<br />
							<span class="tab5"></span>[<strong>otherFee</strong>] => 0.00<br />
							<span class="tab5"></span>[<strong>referencingFee</strong>] => 125.00<br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>epc</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>number</strong>] => 9658-3086-6237-9262-4930<br />
							<span class="tab5"></span>[<strong>energyefficiency_actual</strong>] => E<br />
							<span class="tab5"></span>[<strong>energyefficiency_possible</strong>] => C<br />
							<span class="tab5"></span>[<strong>environmentalimpact_actual</strong>] => E<br />
							<span class="tab5"></span>[<strong>environmentalimpact_possible</strong>] => C<br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>features</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>0</strong>] => Bills Included<br />
							<span class="tab5"></span>[<strong>1</strong>] => Double Bedrooms<br />
							<span class="tab5"></span>[<strong>2</strong>] => Parking<br />
							<span class="tab5"></span>[<strong>3</strong>] => Broadband Internet<br />
							<span class="tab5"></span>[<strong>4</strong>] => Has Garden<br />
							<span class="tab5"></span>[<strong>5</strong>] => Fire alarm<br />
							<span class="tab5"></span>[<strong>6</strong>] => Burglar Alarm<br />
							<span class="tab5"></span>[<strong>7</strong>] => Washing machine<br />
							<span class="tab5"></span>[<strong>8</strong>] => Bath<br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>available</strong>] => 4<br />
					<span class="tab3"></span>[<strong>accredited</strong>] => 1<br />
					<span class="tab3"></span>[<strong>images</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>0</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>original</strong>] => "Property Image 1"<br />
								<span class="tab6"></span>)<br />

						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>description</strong>] => "Property Description"<br />
					<span class="tab3"></span>[<strong>stations</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>Gillingham (kent)</strong>] => 0.6<br />
							<span class="tab5"></span>[<strong>Chatham</strong>] => 2.0<br />
						<span class="tab4"></span>)<span class="tab"></span><br />

					<span class="tab3"></span>[<strong>error</strong>] => False <em>If True the above params will not be returned</em><br />
				<span class="tab2"></span>)<br />
		</p>		
		<p class="v112">
			<h2 class="v112">3.3.2 Get hall data - [action] = "hall" <em>v1.1</em></h2><br />
			This will return all the information for a hall reference by the ID.<br />
			<center><br /><em>Params Sent</em></center>
				<ul>
					<li style="color: #002a8a"><small><strong>client</strong>: your client name</small></li>
					<li style="color: #002a8a"><small><strong>token</strong>: your authentication token / password</small></li>
					<li style="color: #002a8a"><small><strong>action</strong>: the action/call name for the api <em>(eg: 'version', 'search', 'property', 'requestInfo', 'towns')</em></small></li>
					<li style="color: #002a8a"><small><strong>ip</strong>: the IP of the user that is using your software<br />
						We need this to store analytics details about who views what property as well as detecting abuses!!!<br />
						If you do not provide a unique, REAL IP for all your users and one of them commits and abuse we may disable your account instead of disabling access of that IP.
					</small></li>
					<li><strong>id</strong>: Integer. REQUIRED. This is the property ID that you want the details for.</li>
					<li><em>The <u>thumbnails</u> param is not applicable for student halls. Action="hall" will return just the original image, ALWAYS.</em></li>
				</ul>
			<br /><br />
			<center>
				<em>Response Structure [JSON => Array]</em><br />
			</center>
			<span class="tab"></span>Array<br />
				<span class="tab2"></span>(<br />
					<span class="tab3"></span>[<strong>id</strong>] => Integer. Student hall Id<br />
					<span class="tab3"></span>[<strong>name</strong>] => String. Student hall name<br />
					<span class="tab3"></span>[<strong>address</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>number</strong>] => String. Street Number (can contain letters, eg: 12A)<br />
							<span class="tab5"></span>[<strong>street</strong>] => String. Street Name<br />
							<span class="tab5"></span>[<strong>town</strong>] => String. Town<br />
							<span class="tab5"></span>[<strong>county</strong>] => String. County<br />
							<span class="tab5"></span>[<strong>postcode</strong>] => String. Postcode<br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>rent</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>lowest</strong>] => Float. Minimum rent per person per month<br />
							<span class="tab5"></span>[<strong>highest</strong>] => Float. Maximum rent per person per month<br />
							<span class="tab5"></span><em>Student halls do not have a total param, because you can not book an entire hall</em><br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>rooms</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>0</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>id</strong>] => Integer. StudentTenant Room ID<br />
									<span class="tab7"></span>[<strong>name</strong>] => String. Room Name (usually it's bedroom 1, 2, etc...)<br />
									<span class="tab7"></span>[<strong>rent</strong>] => Float. Rent per person per month<br />
									<span class="tab7"></span>[<strong>description</strong>] => String(html). The description of the hall room<br />
									<span class="tab7"></span>[<strong>rentinfo</strong>] => String(html). The description of how the rent should be paid. Eg: some halls might have discounts if you pay in advance or stay longer.<br />
								<span class="tab6"></span>)<br />

							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />
							<span class="tab5"></span><em>------------ All rooms will be listed in the same format -------------</em><br />
							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />

						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>fees</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>referencingFee</strong>] => Float. The StudentTenant referencing fee for this student hall &pounds; per person<br />
						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>features</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>0</strong>] => String. Feature X<br />
							<span class="tab5"></span>[<strong>1</strong>] => String. Feature Y<br />

							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />
							<span class="tab5"></span><em>----------- All features will be listed in the same format ------------</em><br />
							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />
							
						<span class="tab4"></span>)<br />
						
					<span class="tab3"></span>[<strong>images</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>0</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>original</strong>] => String, URL of "Student hall Image 1"<br />
								<span class="tab6"></span>)<br />
							<span class="tab5"></span>[<strong>1</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>original</strong>] => String, URL of "Student hall Image 2"<br />
								<span class="tab6"></span>)<br />
							<span class="tab5"></span>[<strong>2</strong>] => Array<br />
								<span class="tab6"></span>(<br />
									<span class="tab7"></span>[<strong>original</strong>] => String, URL of "Student hall Image 3"<br />
								<span class="tab6"></span>)<br />
							
							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />
							<span class="tab5"></span><em>----------- All images will be listed in the same format ------------</em><br />
							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />

						<span class="tab4"></span>)<br />

					<span class="tab3"></span>[<strong>hallDescription</strong>] => String(bbcode). "Hall Description"<br />
					<span class="tab3"></span>[<strong>areaDescription</strong>] => String(bbcode). "Hall Description"<br />
					<span class="tab3"></span>[<strong>stations</strong>] => Array - This array will display the closest train stations<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>String. Station X Name||Integer. 0=Train/1=Underground/2=Both</strong>] => Integer. Distance in miles from the property<br />
							<span class="tab5"></span>[<strong>String. Station Y Name||Integer. 0=Train/1=Underground/2=Both</strong>] => Integer. Distance in miles from the property<br />
						<span class="tab4"></span>)<span class="tab"></span><br />

					<span class="tab3"></span>[<strong>error</strong>] => False <em>If True the above params will not be returned</em><br />
				<span class="tab2"></span>)<br />
			
			<br />
			
			<center>
				<em>Example Request</em><br />
			</center>
			<span class="tab"></span>Array<br />
				<span class="tab2"></span>(<br />
					<small><span class="tab3"></span>[<strong>action</strong>] => "hall"</small><br />
					<small><span class="tab3"></span>[<strong>ip</strong>] => "127.0.0.1"</small><br />
					<small><span class="tab3"></span>[<strong>client</strong>] => "demo"</small><br />
					<small><span class="tab3"></span>[<strong>token</strong>] => "token"</small><br />
					<span class="tab3"></span>[<strong>id</strong>] => 394<br />
				<span class="tab2"></span>)<br /><br />
				
			<center>
				<em>Example Response</em><br />
			</center>
			<span class="tab"></span>Array<br />
				<span class="tab2"></span>(<br />
					<span class="tab3"></span>[<strong>id</strong>] => 1<br />
					<span class="tab3"></span>[<strong>name</strong>] => Wilmslow<br />
					<span class="tab3"></span>[<strong>address</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>number</strong>] => 211<br />
							<span class="tab5"></span>[<strong>street</strong>] => Hathersage Road<br />
							<span class="tab5"></span>[<strong>town</strong>] => Manchester<br />
							<span class="tab5"></span>[<strong>county</strong>] => Greater Manchester<br />
							<span class="tab5"></span>[<strong>postcode</strong>] => M13 0JQ<br />
						<span class="tab4"></span>)<br />
			<br />
					<span class="tab3"></span>[<strong>rent</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>lowest</strong>] => 434<br />
							<span class="tab5"></span>[<strong>highest</strong>] => 760<br />
						<span class="tab4"></span>)<br />
			<br />
					<span class="tab3"></span>[<strong>rooms</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>0</strong>] => Array<br />
								<span class="tab6">(<br />
									<span class="tab7">[<strong>id</strong>] => 1<br />
									<span class="tab7">[<strong>name</strong>] => Standard Ensuite Classic Apartment<br />
									<span class="tab7">[<strong>rent</strong>] => 434<br />
									<span class="tab7">[<strong>description</strong>] => "html code"<br />
									<span class="tab7">[<strong>rentinfo</strong>] => "html code"<br />
								<span class="tab6">)<br />
			<br />
							<span class="tab5"></span>[<strong>1</strong>] => Array<br />
								<span class="tab6">(<br />
									<span class="tab7">[<strong>id</strong>] => 2<br />
									<span class="tab7">[<strong>name</strong>] => Standard Plus Ensuite Classic Apartment<br />
									<span class="tab7">[<strong>rent</strong>] => 443<br />
									<span class="tab7">[<strong>description</strong>] => "html code"<br />
									<span class="tab7">[<strong>rentinfo</strong>] => "html code"<br />
								<span class="tab6">)<br />
			<br />
							<span class="tab5"></span>[<strong>2</strong>] => Array<br />
								<span class="tab6">(<br />
									<span class="tab7">[<strong>id</strong>] => 3<br />
									<span class="tab7">[<strong>name</strong>] => Deluxe Ensuite Classic Apartment<br />
									<span class="tab7">[<strong>rent</strong>] => 460<br />
									<span class="tab7">[<strong>description</strong>] => "html code"<br />
									<span class="tab7">[<strong>rentinfo</strong>] => "html code"<br />
								<span class="tab6">)<br />
			<br />
							<span class="tab5"></span>[<strong>3</strong>] => Array<br />
								<span class="tab6">(<br />
									<span class="tab7">[<strong>id</strong>] => 4<br />
									<span class="tab7">[<strong>name</strong>] => Studio<br />
									<span class="tab7">[<strong>rent</strong>] => 708<br />
									<span class="tab7">[<strong>description</strong>] => "html code"<br />
									<span class="tab7">[<strong>rentinfo</strong>] => "html code"<br />
								<span class="tab6">)<br />
			<br />
							<span class="tab5"></span>[<strong>4</strong>] => Array<br />
								<span class="tab6">(<br />
									<span class="tab7">[<strong>id</strong>] => 5<br />
									<span class="tab7">[<strong>name</strong>] => 1 Bed Flat<br />
									<span class="tab7">[<strong>rent</strong>] => 760<br />
									<span class="tab7">[<strong>description</strong>] => "html code"<br />
									<span class="tab7">[<strong>rentinfo</strong>] => "html code"<br />
								<span class="tab6">)<br />
			<br />
							<span class="tab5"></span>[<strong>5</strong>] => Array<br />
								<span class="tab6">(<br />
									<span class="tab7">[<strong>id</strong>] => 6<br />
									<span class="tab7">[<strong>name</strong>] => Standard Plus Ensuite Premium Apatment<br />
									<span class="tab7">[<strong>rent</strong>] => 482<br />
									<span class="tab7">[<strong>description</strong>] => "html code"<br />
									<span class="tab7">[<strong>rentinfo</strong>] => "html code"<br />
								<span class="tab6">)<br />
			<br />
							<span class="tab5"></span>[<strong>6</strong>] => Array<br />
								<span class="tab6">(<br />
									<span class="tab7">[<strong>id</strong>] => 7<br />
									<span class="tab7">[<strong>name</strong>] => Deluxe Ensuite Premium Apartment<br />
									<span class="tab7">[<strong>rent</strong>] => 504<br />
									<span class="tab7">[<strong>description</strong>] => "html code"<br />
									<span class="tab7">[<strong>rentinfo</strong>] => "html code"<br />
								<span class="tab6">)<br />
						<span class="tab4"></span>)<br />
			<br />
					<span class="tab3"></span>[<strong>fees</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>referencingFee</strong>] => 125.00<br />
						<span class="tab4"></span>)<br />
			<br />
					<span class="tab3"></span>[<strong>features</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>1</strong>] => Wifi<br />
							<span class="tab5"></span>[<strong>2</strong>] => Bike storage<br />
							<span class="tab5"></span>[<strong>3</strong>] => Internet<br />
							<span class="tab5"></span>[<strong>4</strong>] => CCTV<br />
							<span class="tab5"></span>[<strong>5</strong>] => On site Security<br />
							<span class="tab5"></span>[<strong>6</strong>] => Common Room<br />
							<span class="tab5"></span>[<strong>7</strong>] => Secure door entry<br />
							<span class="tab5"></span>[<strong>8</strong>] => Vending Machine<br />
							<span class="tab5"></span>[<strong>9</strong>] => Laundry<br />
							<span class="tab5"></span>[<strong>10</strong>] => En-suite<br />
							<span class="tab5"></span>[<strong>11</strong>] => Wheelchair accessible<br />
							<span class="tab5"></span>[<strong>12</strong>] => Cash Point<br />
							<span class="tab5"></span>[<strong>13</strong>] => Gym<br />
							<span class="tab5"></span>[<strong>14</strong>] => Swimming Pool<br />
							<span class="tab5"></span>[<strong>15</strong>] => Pool Table<br />
							<span class="tab5"></span>[<strong>16</strong>] => Kitchen<br />
							<span class="tab5"></span>[<strong>17</strong>] => Fridge/Freezer<br />
							<span class="tab5"></span>[<strong>18</strong>] => In Room Phone<br />
							<span class="tab5"></span>[<strong>19</strong>] => TV<br />
							<span class="tab5"></span>[<strong>20</strong>] => Vacuum Cleaner<br />
							<span class="tab5"></span>[<strong>21</strong>] => Iron<br />
							<span class="tab5"></span>[<strong>22</strong>] => On site Maintenance<br />
							<span class="tab5"></span>[<strong>23</strong>] => Photocopier<br />
							<span class="tab5"></span>[<strong>24</strong>] => Car Parking<br />
						<span class="tab4"></span>)<br />
			<br />
					<span class="tab3"></span>[<strong>images</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>0</strong>] => Array<br />
								<span class="tab6">(<br />
									<span class="tab7">[<strong>original</strong>] => http://www.wilmslowparkstudents.com/upl_images/pg_a_60.jpg<br />
								<span class="tab6">)<br />
						<span class="tab4"></span>)<br />
			<br />
					<span class="tab3"></span>[<strong>hallDescription</strong>] => "[h2]Headline[/h2] normal text [b]bold text[/b] [i]italic text[/i]"<br />
					<span class="tab3"></span>[<strong>areaDescription</strong>] => "[h2]Headline[/h2] normal text [b]bold text[/b] [i]italic text[/i]"<br />
					<span class="tab3"></span>[<strong>stations</strong>] => Array<br />
						<span class="tab4"></span>(<br />
							<span class="tab5"></span>[<strong>Ardwick||0</strong>] => 1.0<br />
							<span class="tab5"></span>[<strong>Manchester Piccadilly||0</strong>] => 1.4<br />
							<span class="tab5"></span>[<strong>Manchester Oxford Road||0</strong>] => 1.4<br />
							<span class="tab5"></span>[<strong>Deansgate||0</strong>] => 1.5<br />
						<span class="tab4"></span>)<br />
			<br />
					<span class="tab3"></span>[<strong>error</strong>] => <br />
				<span class="tab2"></span>)
		</p>
		<p>
			<h2>3.4 Make a request - [action] = "requestInfo"</h2><br />
			If you want your users to be able to request property information from our website, you need to call the action "requestInfo" and provide the required parameters<br /><br />
						
			<center>
				<em>Params Sent</em><br />
			</center>
			<span class="tab"></span>Array<br />
				<span class="tab2"></span>(<br />
					<small><span class="tab3"></span>[<strong>action</strong>] => "requestInfo"</small><br />
					<small><span class="tab3"></span>[<strong>ip</strong>] => "127.0.0.1"</small><br />
					<small><span class="tab3"></span>[<strong>client</strong>] => "demo"</small><br />
					<small><span class="tab3"></span>[<strong>token</strong>] => "token"</small><br />
					<span class="tab3"></span>[<strong>property</strong>] => Integer, ID of the property your user requested info on<br />
					<span class="v112"><span class="tab3"></span>[v1.1: <strong>hall</strong>] => Integer, ID of the student hall your user requested info on</span><br />
					<span class="tab3"></span>[<strong>username</strong>] => String. Name of the user that requested the info. Please advise your users to use their legal name(full real name)<br />
					<span class="tab3"></span>[<strong>email</strong>] => String. Email of the user. Please make sure it is validated.<br />
					<span class="tab3"></span>[<strong>group</strong>] => Integer. Not required. Default = 1. How many people are interested in this property. If the user is a lead tenant on behalf of a group, please advise your users to specify the group size<br />
					<span class="tab3"></span>[<strong>message</strong>] => String. The message your user wants to send to the landlord/agent of the property. Please advise users to write their messages in English. Eg: "I would like to view this property, can you tell me when you are available for a viewing?"<br />
				<span class="tab2"></span>)<br /><br />
				
			<center>
				<em>Array Returned</em><br />
			</center>
			
			<span class="tab"></span>Array<br />
				<span class="tab2"></span>(<br />
					<span class="tab3"></span>[<strong>message</strong>] => String. Announces you that the message was sent in the form of a string. <br />
					<span class="tab3"></span>[<strong>ok</strong>] => Boolean. True if the message was sent, false if it was not sent. <br />
					<span class="tab3"></span>[<strong>error</strong>] => Boolean. True if there was an error in your API request, False if the request to the API was sent correctly. <br />
					<span class="v112"><span class="tab3"></span>[v1.1: <strong>property</strong>] => Integer. If this is a request info for a property, the property id will be returned as well </span><br />
					<span class="v112"><span class="tab3"></span>[v1.1: <strong>hall</strong>] => Integer. If this is a request info for a student hall, the hall id will be returned as well </span><br />
				<span class="tab2"></span>)<br /><br />
		</p>
		<p>
			<h2>3.5 Get all town list - [action] = "towns"</h2><br />
			If you want to offer a <em>select</em> form or an <em>autocomplete</em> form for your search widget that includes the towns name, you can get a list of all towns where the API will return results.<br /><br />
			This request is simple, the only thing you need change in your request is the action to "towns" and you will get the full list of towns.<br /><br />
						
			<center>
				<em>Params Sent</em><br />
			</center>
			<span class="tab"></span>Array<br />
				<span class="tab2"></span>(<br />
					<small><span class="tab3"></span>[<strong>action</strong>] => "towns"</small><br />
					<small><span class="tab3"></span>[<strong>ip</strong>] => "127.0.0.1"</small><br />
					<small><span class="tab3"></span>[<strong>client</strong>] => "demo"</small><br />
					<small><span class="tab3"></span>[<strong>token</strong>] => "token"</small><br />
				<span class="tab2"></span>)<br /><br />
				
			
			<center>
				<em>Array Returned</em><br />
			</center>
				
			<span class="tab">Array<br />
				<span class="tab2">(<br />
					<span class="tab3">[towns] => Array<br />
						<span class="tab4">(<br />
							<span class="tab5">[0] => Town 1<br />
							<span class="tab5">[1] => Town 2<br />							
							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />
							<span class="tab5"></span><em>----------- All towns will be listed in the same format ------------</em><br />
							<span class="tab5"></span><em>-----------------------------------------------------------------------------------</em><br />
						<span class="tab4">)<br />
					<span class="tab3">[error] => True if there were any errors (eg: Invalid client authentication) or False if there were no errors<br />
				<span class="tab2">)<br />
		</p>
		<p>
			<h2 class="v112">4. Error tracing and debugging + upgrading notice <em>v1.11</em></h2><br />
			In this demo website & in the <strong>studenttenant.sdk.php v1.3</strong> we implemented advance error tracing and debugging + upgrading notice.<br />
		</p>
		<p>
			<h2 class="v112">4.1 Error tracing and debugging<em>v1.11</em></h4><br />
			In <strong>system/function.php</strong> we added 2 new functions <strong>stErrorHandler</strong> and <strong>stExceptionHandler</strong> that are overwriting the normal errors/exceptions if we are in the production enviroment.
			Please check those functions as well as the following code: <br /><br />
			<img src="images/6a1d1138d0626e0816e5c32137d09763.png" /><br /><br />
			These functions will use the HTML file located in the root of this demo website called <strong>error.htm</strong>.<br /><br />
			You can amend that in any way you like, but it is advisable that you will still keep the following parameters to help you and your users with debugging this:
				<ul>
					<li>[<strong>ERROR-NUMBER</strong>] - The error number</li>
					<li>[<strong>ERROR-TAG</strong>] - The error short description</li>
					<li>[<strong>ERROR-MESSAGE</strong>] - The long description of the error</li>					
				</ul><br />
			Feel free to amend the functions to log the errors as well. This however will be available in a later version of the demo website.
		</p>
		<p>
			<h2 class="v112">4.2 Upgrading notice<em>v1.11</em></h4><br />
			StudentTenant is constantly under development to help with our aim to offer the best services both to our clients as well as our partners API and their clients. <br /><br />
			Because of this our website - including our API - is down while the update takes place. <br /><br />
			The upgrading process consists into getting the new source code from our production branch and recreating the cache for our assets and even pre-compile our code to facilitate the best possible request time. <br />
			While the upgrading is under processing(usually takes from 30 seconds to 1 minute), our users will see an upgrading screen. In <strong>studenttenant.sdk.php v1.3</strong> we implemented a special exception that is raised when this process occurs. This exception is used by the <strong>stExceptionHandler</strong> mentioned in this documentation at 4.1 to display a similar, user friendly message for our API partners clients.<br /><br />
			This simply returns the HTML file located in the root of this demo website called <strong>upgrading.htm</strong>.<br /><br />
			The page automatically refresh every 5 seconds, assuring the user will not need to take ANY actions to proceed with his request.<br /><br />
			E.g.: If a user tried to do a search and pressed the search button after our server started to upgrade, this page will display while the upgrade process is running and after that the user will see his search results without having to complete the search params again, press search or even refresh.<br />
			This assures continuity even when we upgrade the servers.
		</p>
		<p>
			<h2 class="v112">5. Image caching <em>v1.12</em></h2><br />
			Serving images to users as close to their location as possible is vital for the speed of an website.<br /><br />
			Another important aspect that affects the speed of the website is loading the appropriate image size. Why load an 12MP image if you only display 1.3MP?<br /><br />
			To help with this we implemented in the demo website an resize library and a <strong>image</strong> function that will cache all the images of our properties.<br /><br />
			How does it work?
				<ol>
					<li>The user is loading a property page</li>
					<li>Your server receives the property information, including the URL of the property images</li>
					<li>Your server checks if there is a cache for the size of the image that needs to be displayed <em>(eg: 282x212)</em>
						<ul>
							<li>If the cache exists, it returns that cache rather than the original url</li>
							<li>If the cache does not exist the following process occurs:
								<ol>
									<li>The server checks if the original image cache exists
										<ul>
											<li>If the image cache of the original exists, it takes no action</li>
											<li>If the image cache of the original does not exist, your server will download it</li>
										</ul>
									</li>
									<li>Using the <strong>system/resize.class.php</strong> your server will create the cache at the size requested <em>(eg: 282x212)</em></li>
								</ol>
							</li>
						</ul>
					</li>
					<li>Your user gets the image cache URL from your server</li>
				</ol>
				<br /><br />
				<strong>Advantages: </strong>
					<ul>
						<li>Lower bandwidth usage for your user</li>
						<li>Faster, location-based loading for your user</li>
						<li>Less stress on the API</li>
					</ul>
				<br /><br />
				<strong>Disadvantage: </strong>
					<ul>
						<li>The first time an image that has no cache is requested, the loading time will be higher</li>
					</ul>
				<br /><br />
				We are working to release a new version of the DEMO Website that will include an implementation that removes the disadvantage. All our clients will be notified when this happens.<br /><br />
				Config file that needs to be setup: <br /><br />
				<img src="images/2a964251ad9a27b123ecc2a658e1ed6d.png" />
				
		</p>
		<p>
			<h2>6. Things to remember</h2><br />
			<ol>
				<li>Include the authentication details with all the requests (client/token parameters)</li>
				<li>Include the IP address of your user with all the requests (ip parameter) - Otherwise in case one of your users abuses our API all the others will have to suffer</li>
				<li>All requests will be defined by the parameter "action"</li>
				<li>Actions may have other required parameters, please create all requests as explained in this document</li>
				<li>If your website is built in PHP, it is highly advisable you would use our PHP SDK, if not feel free to ask our technical support to port this for your solution</li>
				<li>The DEMO Website source code is yours to analyse and provides valuable help in implementing the API on your website</li>
				<li><strong>Feel free to contact our technical support at any time!</strong></li>
			</ol>
		</p>
		<div class="center_left_box">
			<div class="box_title"><span>API</span> Technical support:</div>
			<div class="form">
				<div class="form_row"  style="width: 350px"> <img src="images/contact_envelope.gif" width="50" height="47" border="0" class="img_right" alt="">
					<div class="contact_information" style="font-size: 18px;">
						Email: romeo@jakuta.co.uk<br>
						Telephone: +447 85 85 427 99<br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>