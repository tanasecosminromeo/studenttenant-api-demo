<?php	
	if (isset($_GET['property'])){
		$p 	  = $st->call("property", array("id"=>$_GET['property']));  
	} elseif (isset($_GET['hall'])){
		$h 	  = $st->call("hall", array("id"=>$_GET['hall']));  
	} else {
		/* Error */
	}
?>
<div id="main_content">
	<?php include "_sidebar.php"; ?>
	<div class="column4">
		<div class="title">Request Info for <?php if (isset($h)){ print $h['name']; } else { print $p['address']['number']." ".$p['address']['street'].", ".$p['address']['town']; } ?></div>
	</div>
	<div class="column2" style="background-color:#f3f5f6; margin-left:5px;">
		<?php
			if (isset($h)){ $image = @$h['images'][0]['original']; } else { $image = @$p['images'][0]['original']; }
			if (!strstr($image, "http")){ $image = "images/no-photo-130.jpg"; }
		?>
		<div class="big_pic"><img src="<?php print image($image, 282, 212); ?>" width="282" height="212" alt="" class="img_big_pic" /></div>
		<div class="main_text_box">
			<h1>Description</h1>
			<p>
				<?php 
					if (isset($h)){						
						$hallDescription = $h['hallDescription'];
						$hallDescription = str_replace(array("[", "]"), array("<", ">"), $hallDescription);
						
						print $hallDescription;
					} else { 
						print $p['description']; 
					} 
				?>
			</p>
		</div>
	</div>
	<div class="column3">
		<div class="main_text_box">
			<h1>Request Info</h1>
			
			<?php if (!isset($_POST['submit'])): ?>
			<?php if (isset($h)): ?>
			<form action="index.php?page=requestinfo&hall=<?php print $h['id']; ?>" method="POST">
			<?php else: ?>
			<form action="index.php?page=requestinfo&property=<?php print $p['id']; ?>" method="POST">
			<?php endif; ?>
				<strong>Username*: </strong><input type="text" name="username" /><br />
				<strong>Email*: </strong><input type="text" name="email" /><br />
				<strong>Phone: </strong><input type="text" name="phone" /><br />
				<strong>Group Size: </strong><input type="text" name="group" /><br />
				<strong>Message*: </strong>
					<textarea name="message">
						
					</textarea><br />
				<p>&nbsp;</p>
				<input type="submit" name="submit" value="Request Info" />
			</form>
			<?php
				else:
				
				$params = array(
						"username" => addslashes($_POST['username']), #The user's full name. Please advise users to use their legal name
						"email"    => addslashes($_POST['email']), #The user's email. Our system will check if that is valid and if it's not a temp email. Please take measures as well.
						"phone"    => addslashes($_POST['phone']), #The user's phone number. Although they are not required please advise users to add their phone number. Also, if it's an international number don't forget to add the international prefix
						"group"    => intval($_POST['group']), #How many people are interested in this property. If the user is a lead tenant on behalf of a group, please advise your users to specify the group size
						"message"  => addslashes($_POST['message']) #This is the message that your users wants to send to the landlord. Please advise your users to write messages in English our provide some kind of translation before that.
					);
					
				if (isset($h)){
					$params["hall"] 	= $h['id']; #Hall ID. Version 1.1					
				} else {
					$params["property"] = $p['id']; #Property ID
				}
				
				$return = $st->call("requestInfo", $params);
				
				if (@$return['ok']==true){
					echo "<h2>Message was sent!</h2>";
				} else {
					echo "<h2 style='color: #FF0000'>There was an error sending the message.<br />Please make sure you complete all the required fields.</h2>";
				}

				if (env=="dev"){
					echo "<p>&nbsp;</p><p>&nbsp;</p><hr /><strong>Params sent: </strong>"; 
					print_r($params);
					echo "<p>&nbsp;</p><strong>Params Returned: </strong>"; 
					print_r($return);
				}
				
				endif;
			?>
			<p style="clear:both;">&nbsp;</p>
			<div style="float:left;">
				<?php if (isset($h)): ?>
				<div class="button"><a href="index.php?page=hall&id=<?php print $h['id']; ?>">Student Hall Details</a></div>
				<?php else: ?>
				<div class="button"><a href="index.php?page=property&id=<?php print $p['id']; ?>">Property Details</a></div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>