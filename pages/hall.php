<?php	
	$h = $st->call("hall", array("id"=>$_GET['id']));  
?>
<div id="main_content">
	<?php include "_sidebar.php"; ?>
	<div class="column4">
		<div class="title"><?php print $h['name']."<br /><small>".$h['address']['number']." ".$h['address']['street'].", ".$h['address']['town']."</small>"; ?></div>
	</div>
	<div class="column2" style="background-color:#f3f5f6; margin-left:5px;">
		<?php
			$image = @$h['images'][0]['original'];
			if (!strstr($image, "http")){ $image = "images/no-photo-130.jpg"; }
		?>
		<div class="big_pic"><img src="<?php print image($image, 282, 212); ?>" width="282" height="212" alt="" class="img_big_pic" /></div>
		<div class="pictures_thumbs">
			<h3>Pictures available:</h3>
			<?php 
				foreach ($h['images'] as $i){
					print '<a href="#"><img src="'.image($i['original'], 104, 78).'" width="104" height="78" border="0" alt="" class="img_thumb" /></a>';
				}
			?>
		</div>
		<p style="clear: both;">&nbsp;</p>
		<div class="pictures_thumbs">
			<h3>Full Address:</h3>
			<strong>Street: </strong> <?php print $h['address']['number']." ".$h['address']['street']; ?><br />
			<strong>Town: </strong> <?php print $h['address']['town']; ?><br />
			<strong>County: </strong> <?php print $h['address']['county']; ?><br />
			<strong>Postcode: </strong> <?php print $h['address']['postcode']; ?>
		</div>
		<p style="clear: both;">&nbsp;</p>
		<div class="pictures_thumbs">
			<h3>Nearby Train Stations:</h3>
			<?php foreach ($h['stations'] as $sn => $sd): ?>
			<strong><?php print $sn; ?>: </strong> <?php print $sd; ?> miles<br />
			<?php endforeach; ?>
		</div>
		<p style="clear: both;">&nbsp;</p>
		<div class="pictures_thumbs">
			<h3>Features:</h3>
			<ul>
			<?php foreach ($h['features'] as $f): ?>
				<li><?php print $f; ?></li>
			<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<div class="column3">
		<div class="main_text_box">
			<h1>Description</h1>
			<p>
				<?php
					$hallDescription = $h['hallDescription'];
					$hallDescription = str_replace(array("[", "]"), array("<", ">"), $hallDescription);
					
					print $hallDescription;
				?>
			</p>
		</div>
		<div class="title2">Room Types:</div>
		<div class="details_list">
			<ul>
			<?php foreach ($h['rooms'] as $r): ?>
				<li><?php print $r['name'].": &pound;".$r['rent']; ?></li>
			<?php endforeach; ?>
			</ul>
		</div>
		<div class="title2">Fees:</div>
		<div class="details_list">
			<ul>
				<li><span>Referencing Fee:</span> &pound;<?php print $h['fees']['referencingFee']; ?></li>
			</ul>
		</div>
		<div style="float:left;">
			<div class="button"><a href="index.php?page=requestinfo&hall=<?php print $h['id']; ?>">I'm Interested</a></div>
		</div>
		<div style="float:left; padding-left:150px;"><a href="#"><img src="images/print.gif" width="28" height="28" border="0" alt="print this offer" title="print this offer" /></a> </div>
	</div>
</div>