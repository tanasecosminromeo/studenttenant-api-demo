<?php	
	$p = $st->call("property", array("id"=>$_GET['id']));  
?>
<div id="main_content">
	<?php include "_sidebar.php"; ?>
	<div class="column4">
		<div class="title"><?php print $p['address']['number']." ".$p['address']['street'].", ".$p['address']['town']; ?></div>
	</div>
	<div class="column2" style="background-color:#f3f5f6; margin-left:5px;">
		<?php
			$image = @$p['images'][0]['original'];
			if (!strstr($image, "http")){ $image = "images/no-photo-130.jpg"; }
		?>
		<div class="big_pic"><img src="<?php print image($image, 282, 212); ?>" width="282" height="212" alt="" class="img_big_pic" /></div>
		<div class="pictures_thumbs">
			<h3>Pictures available:</h3>
			<?php 
				foreach ($p['images'] as $i){
					print '<a href="#"><img src="'.image($i['original'], 104, 78).'" width="104" height="78" border="0" alt="" class="img_thumb" /></a>';
				}
			?>
		</div>
		<p style="clear: both;">&nbsp;</p>
		<div class="pictures_thumbs">
			<h3>Full Address:</h3>
			<strong>Street: </strong> <?php print $p['address']['number']." ".$p['address']['street']; ?><br />
			<strong>Town: </strong> <?php print $p['address']['town']; ?><br />
			<strong>County: </strong> <?php print $p['address']['county']; ?><br />
			<strong>Postcode: </strong> <?php print $p['address']['postcode']; ?>
		</div>
		<p style="clear: both;">&nbsp;</p>
		<div class="pictures_thumbs">
			<h3>Nearby Train Stations:</h3>
			<?php foreach ($p['stations'] as $sn => $sd): ?>
			<strong><?php print $sn; ?>: </strong> <?php print $sd; ?> miles<br />
			<?php endforeach; ?>
		</div>
		<p style="clear: both;">&nbsp;</p>
		<div class="pictures_thumbs">
			<h3>Features:</h3>
			<ul>
			<?php foreach ($p['features'] as $f): ?>
				<li><?php print $f; ?></li>
			<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<div class="column3">
		<div class="main_text_box">
			<h1>Description</h1>
			<p><?php print $p['description']; ?></p>
		</div>
		<div class="title2">Info:</div>
		<div class="details_list">
			<ul>
				<li><span>Bedrooms Available:</span> <?php print intval($p['available']); ?></li>
				<li><span>Accredited?:</span> <?php if ($p['accredited']==true){ print "<span style='color: #00FF00;'>YES</span>"; } else { print "<span style='color: #FF0000;'>NO</span>"; } ?> </li>
			</ul>
		</div>
		<div class="title2">Rent:</div>
		<div class="details_list">
			<ul>
				<li><span>Lowest:</span> &pound;<?php print intval($p['rent']['lowest']); ?>pppm</li>
				<li><span>Total:</span> &pound;<?php print $p['rent']['total']; ?>pcm</li>
			</ul>
		</div>
		<div class="title2">Fees:</div>
		<div class="details_list">
			<ul>
				<li><span>Referencing Fee:</span> &pound;<?php print $p['fees']['referencingFee']; ?></li>
				<li><span>Administration Fee:</span> &pound;<?php print $p['fees']['administrationFee']; ?></li>
				<li><span>Deposit:</span> &pound;<?php print $p['fees']['deposit']; ?></li>
				<li><span>Other Fee:</span> &pound;<?php print $p['fees']['otherFee']; ?></li>
			</ul>
		</div>
		<div class="title2">Bedrooms:</div>
		<div class="details_list">
			<ul>
				<?php foreach ($p['bedrooms'] as $b): ?>
				<li>
					<span><?php print $b['name'] ?>:</span> 
					&pound;<?php print intval($b['rent']); ?>pppm 
						<small> 
							<?php if ($b['ensuite']){ print "Ensuite"; } else { print "Shared bathroom"; } ?> / 
							<?php if ($b['available']){ print "<span style='color: #00FF00;'>Available</span>"; } else { print "<span style='color: #FF0000;'>Unavailable</span>"; } ?> 
						</small>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="title2">EPC Certificate:</div>
		<div class="details_list">
			<?php if (sizeof($p['epc'])>0): ?>
			<ul>
				<li><span>Number:</span> <?php print $p['epc']['number']; ?></li>
				<li><span>Energy Efficiency Actual:</span> <?php print $p['epc']['energyefficiency_actual']; ?></li>
				<li><span>Energy Efficiency Possible:</span> <?php print $p['epc']['energyefficiency_possible']; ?></li>
				<li><span>Environmental Impact Actual:</span> <?php print $p['epc']['environmentalimpact_actual']; ?></li>
				<li><span>Environmental Impact Possible:</span> <?php print $p['epc']['environmentalimpact_possible']; ?></li>
			</ul>
			<?php else: ?>
			No EPC info
			<?php endif; ?>
		</div>
		<div style="float:left;">
			<div class="button"><a href="index.php?page=requestinfo&property=<?php print $p['id']; ?>">I'm Interested</a></div>
		</div>
		<div style="float:left; padding-left:150px;"><a href="#"><img src="images/print.gif" width="28" height="28" border="0" alt="print this offer" title="print this offer" /></a> </div>
	</div>
</div>