<div id="main_content">
	<?php include "_sidebar.php"; ?>
	<div class="column2">
		<div class="main_text_box">
			<h2>About Us</h2>
			<p> &quot;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <br />
			<br />
			Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot; </p>
		</div>
		<div class="main_text_box">
			<h2>Request a proposal</h2>
			<div class="proposal">
				<p class="proposal_text">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
			</div>
		</div>
	</div>
	<div class="column3">
		<?php $randProperties = load10RandomProperties(); ?>
		<div class="small_title">Latest offers</div>
		<?php offer_box($randProperties[0]); ?>
		<?php offer_box($randProperties[1]); ?>
	</div>
	<div class="column4">
	<div class="title">Special Offers</div>
		<?php offer_box($randProperties[2], "offer_box_wide"); ?>
		<?php offer_box($randProperties[3], "offer_box_wide"); ?>
		<?php offer_box($randProperties[4], "offer_box_wide"); ?>
		<?php offer_box($randProperties[5], "offer_box_wide"); ?>
	</div>
</div>