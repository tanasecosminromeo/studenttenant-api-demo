<?php
	$townList = $st->call("towns"); //Get a list of all towns;
?>
<div class="column1">
	<!-- Start of Search Widget -->
	<div class="left_box">
		<div class="top_left_box"> </div>
		<div class="center_left_box">
			<div class="box_title"><span>Find</span> your place:</div>
			<div class="form">
				<form action="index.php?page=search" method="POST">
					<div class="form_row">
						<label class="left">Location: </label><input name="location" type="text" class="form_input" value="<?php print @$searchParams['location']; ?>" />
					</div>
					<div class="form_row">
						<label class="left">Town: </label>
						<select name="town" class="form_input">
							<option value="">All Towns</option>
							<?php foreach ($townList['towns'] as $t): ?>
							<option value="<?php print strtolower($t); ?>" <?php if (@$searchParams['town']==strtolower($t)){ print 'selected="selected"'; } ?>><?php print $t; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form_row">
						<label class="left">Rent Minimum: </label><input type="text" name="minRent" class="form_input" value="<?php print intval(@$searchParams['minRent']); ?>" />
					</div>
					<div class="form_row">
						<label class="left">Rent Maximum: </label><input type="text" name="maxRent" class="form_input" value="<?php if (intval(@$searchParams['maxRent'])<100){ print "999999"; } else { print intval(@$searchParams['maxRent']); } ?>" />
					</div>
					<div class="form_row">
						<label class="left">Bedrooms: </label>
						<select name="bedrooms" class="form_input">
							<option value="0" <?php if (intval(@$searchParams['bedrooms'])==0){ print 'selected="selected"'; } ?>>All Bedrooms</option>
							<option value="1" <?php if (intval(@$searchParams['bedrooms'])==1){ print 'selected="selected"'; } ?>>1 Bedroom</option>
							<option value="2" <?php if (intval(@$searchParams['bedrooms'])==2){ print 'selected="selected"'; } ?>>2 Bedrooms</option>
							<option value="3" <?php if (intval(@$searchParams['bedrooms'])==3){ print 'selected="selected"'; } ?>>3 Bedrooms</option>
							<option value="4" <?php if (intval(@$searchParams['bedrooms'])==4){ print 'selected="selected"'; } ?>>4 Bedrooms</option>
							<option value="5" <?php if (intval(@$searchParams['bedrooms'])==5){ print 'selected="selected"'; } ?>>5 Bedrooms</option>
							<option value="6" <?php if (intval(@$searchParams['bedrooms'])==6){ print 'selected="selected"'; } ?>>6 Bedrooms</option>
							<option value="7" <?php if (intval(@$searchParams['bedrooms'])==7){ print 'selected="selected"'; } ?>>7 Bedrooms</option>
							<option value="8" <?php if (intval(@$searchParams['bedrooms'])==8){ print 'selected="selected"'; } ?>>8 Bedrooms</option>
							<option value="9" <?php if (intval(@$searchParams['bedrooms'])==9){ print 'selected="selected"'; } ?>>9 Bedrooms</option>
						</select>
					</div>
					<div class="form_row">
						<label class="left">Only with images: </label><input name="images" value="1" type="checkbox" class="form_input"  <?php if (@$searchParams['images']==true){ print 'checked="checked"'; } ?> />
					</div>
					<div class="form_row">
						<label class="left">Only with Virtual Tours: </label><input name="virtualtour" value="1" type="checkbox" class="form_input" <?php if (@$searchParams['virtualtour']==true){ print 'checked="checked"'; } ?> />
					</div>
					<div class="form_row">
						<label class="left">Only Accredited: </label><input name="accredited" value="1" type="checkbox" class="form_input" <?php if (@$searchParams['accredited']==true){ print 'checked="checked"'; } ?> />
					</div>
					<div class="form_row">
						<label class="left">Include Lodgings: </label><input name="lodgings" value="1" type="checkbox" class="form_input" <?php if (@$searchParams['lodgings']==true){ print 'checked="checked"'; } ?> />
					</div>
					<div class="form_row">
						<label class="left">Include Unavailable: </label><input name="unavailable" value="1" type="checkbox" class="form_input" <?php if (@$searchParams['unavailable']==true){ print 'checked="checked"'; } ?> />
					</div>
					<div class="form_row">
						<label class="left">Sort by: </label>
						<select name="sort" class="form_input">
							<option value="1" <?php if (intval(@$searchParams['sort'])==1){ print 'selected="selected"'; } ?>>Default</option>
							<option value="2" <?php if (intval(@$searchParams['sort'])==2){ print 'selected="selected"'; } ?>>Most Recent</option>
							<option value="3" <?php if (intval(@$searchParams['sort'])==3){ print 'selected="selected"'; } ?>>Lowest price</option>
							<option value="4" <?php if (intval(@$searchParams['sort'])==4){ print 'selected="selected"'; } ?>>Highest Price</option>
							<option value="5" <?php if (intval(@$searchParams['sort'])==5){ print 'selected="selected"'; } ?>>Latest updated</option>
						</select>
					</div>
					<div class="form_row">
						<label class="left">Include Properties: </label><input name="showProperties" value="1" type="checkbox" class="form_input" <?php if (@$searchParams['showProperties']!=false || !isset($searchParams['showProperties'])){ print 'checked="checked"'; } ?> />
					</div>
					<div class="form_row">
						<label class="left">Include Student Halls: </label><input name="showStudentHalls" value="1" type="checkbox" class="form_input" <?php if (@$searchParams['showStudentHalls']!=false || !isset($searchParams['showProperties'])){ print 'checked="checked"'; } ?> />
					</div>

					<div style="float:right; padding:10px 25px 0 0;">
						<input type="image" src="images/find.gif" />
					</div>
				</form>
			</div>
		</div>
		<div class="bottom_left_box"> </div>
	</div>
	<!-- End of Search Widget -->
	<div class="left_box">
		<div class="top_left_box"> </div>
		<div class="center_left_box">
			<div class="box_title"><span>API</span> Technical support:</div>
			<div class="form">
				<div class="form_row"> <img src="images/contact_envelope.gif" width="50" height="47" border="0" class="img_right" alt="" />
					<div class="contact_information">
						Email: romeo@jakuta.co.uk<br />
						Telephone: +447 85 85 427 99<br />
						<br />
						<span>jakuta.co.uk</span> 
					</div>
				</div>
			</div>
		</div>
		<div class="bottom_left_box"> </div>
	</div>
</div>