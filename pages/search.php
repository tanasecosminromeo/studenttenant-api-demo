<?php
	if (isset($_GET['pageNo'])){ $page = intval($_GET['pageNo']); } else { $page = 1; }
	$pagelen = 10;

	if (!isset($_GET['searchid'])){
		$searchParams = array(
			"bedrooms" => intval($_POST['bedrooms']), # Default is show all
			"location" => @$_POST['location'], #Default is null. Can be a street name or a postcode. Not case sensitive
			"town"     => @$_POST['town'], #Default is null. Not case sensitive
			"maxRent"  => intval(@$_POST['maxRent']), #Rent per person per month. Default is 0	
			"minRent"  => intval(@$_POST['minRent']), #Rent per person per month. Default is 99999
			"images"   => (@$_POST['images']==1)?true:false, #Show only properties with images. Default false
			"virtualtour" => (@$_POST['virtualtour']==1)?true:false, #Show only properties with virtual tours. Default is false
			"accredited"  => (@$_POST['accredited']==1)?true:false, #Show only accredited properties. Default is false
			"lodgings"    => (@$_POST['lodgings']==1)?true:false, #Include lodgings. Default is false
			"unavailable"   => (@$_POST['unavailable']==1)?true:false, #Include unavailable properties. Default is false
			"description" => true, #Get property description. Default is false.
			"page" => $page, #Default is 1
			"pagelen" => $pagelen, #Default is 20
			"sort" => intval(@$_POST['sort']), #Sort by: 1=Default, 2=Most Recent, 3=Lowest price, 4=Highest Price, 5=Latest updated. Default is 1.
			"showProperties"   => (@$_POST['showProperties']==1)?true:false, #Include Properties. Default is true
			"showStudentHalls" => (@$_POST['showStudentHalls']==1)?true:false, #Include Student Halls. Default is false	(in the API, not in the demo website, in the demo website default is true)		
		);
	} else {
		$searchParams = array(
			"searchid" => $_GET['searchid'],
			"page" => $page, #Default is 1
			"pagelen" => $pagelen, #Default is 20					
		);
	}			

	$searchList = $st->call("search", $searchParams);  
	
	if (isset($_GET['searchid'])){ $searchParams = $searchList['searchParams']; }
?>
<div id="main_content">
	<?php include "_sidebar.php"; ?>
	<div class="column4">
		<div class="title" style="float:left;">
			<div style="float:left;">Property Search</div>
		</div>

		<?php
			if (sizeof($searchList['properties'])>0 || sizeof($searchList['halls'])>0){
				foreach ($searchList['properties'] as $p){ offer_box($p, "offer_box_wide"); }
				foreach ($searchList['halls'] as $h){ offer_box_hall($h, "offer_box_wide"); }
				
				$maxpage = intval($searchList['total']/$searchList['pagelen']);
				if ($searchList['total'] % $searchList['pagelen'] > 0){ $maxpage++; }
				
				echo '<p style="clear: both;">&nbsp;</p><center><div class="pagination">';
					if ($searchList['page']==1){ echo '<span class="disabled"><<</span>'; } else { echo '<a href="index.php?page=search&pageNo='.($searchList['page']-1).'&searchid='.$searchList['searchid'].'"><<</a>'; }
					for ($i=1; $i<=$maxpage; $i++){
						if ($searchList['page']==$i){ 
							echo '<span class="current">'.$i.'</span>';
						} else {
							echo '<a href="index.php?page=search&pageNo='.$i.'&searchid='.$searchList['searchid'].'">'.$i.'</a>';
						}
					}
					if ($searchList['page']<$maxpage){ echo '<span class="disabled">>></span>'; } else { echo '<a href="index.php?page=search&pageNo='.($searchList['page']+1).'&searchid='.$searchList['searchid'].'">>></a>'; }
				echo '</div></center>';
			} else { 
				print "<h2>No properties found!</h2><br />"; 
			}

			if (env=="dev"){
				echo "<p style='clear: both;'>&nbsp;</p><p>&nbsp;</p><hr /><strong>Params sent: </strong>"; 
				print_r($searchParams);
				echo "<p>&nbsp;</p><strong>Params Returned: </strong>"; 
				print_r($searchList);
			}
		?>
	</div>
</div>
